create database sophia;

use sophia;

create table teams ( team_id int(30) NOT NULL AUTO_INCREMENT, team_name varchar(255), team_password varchar(255), team_members varchar(255), PRIMARY KEY(team_id) );
create table users ( user_id int(30) NOT NULL AUTO_INCREMENT, username varchar(255), password varchar(255), PRIMARY KEY (user_id) );
create table questions ( question varchar(255), ans varchar(255), ans_time datetime);

