<?php
require '../classes/Database.class.php';
//connectDB is used to connect to a database
//It returns a connection

//checkAdminCredentials checks if inserted admin password  and username are co rrect
//It returns a TRUE if credentials are correct FALSE is   they are not correct
function checkAdminCredentials($username, $password){
    $db = new Database();
    $username = addslashes($username);

    $isCorrect = false;
    $result = $db->query("SELECT password FROM users WHERE username = '$username'", false);

    if($result && password_verify($password, $result['0']['password'])) {
        $isCorrect = true;
    } else {
        $isCorrect = false;
    }
    return $isCorrect;
}

function addmembers($team_id, $members) {
    $db = new Database();
    $team_id = addslashes($team_id);
    $members = addslashes($members);
    $db->update("teams", array("members" => $members), "tid = '$team_id'");
    return false;
}

//checkTeamCredentials checks if entered team credentials are correct
//It returns a TRUE if credentials are correct FALSE is they are not correct
function checkTeamCredentials($teamname, $password)
{
    $db = new Database();
    $teamname = addslashes($teamname);
    $password = addslashes($password);

    $isCorrect = false;

    $result = $db->query("SELECT * FROM teams WHERE name = '$teamname' and password = '$password'", true);

    if($result && sizeof($result) > 0) {
        $isCorrect = true;
        return $result;
    }
    return $isCorrect;
}
//createTeam creates a name with given teamname and password if a team with the same name doesn't alreay exsist
function createTeam($teamname,$teampassword)
{
    $db = new Database();
    $teamname = addslashes($teamname);
    $teampassword = addslashes($teampassword);
    $sqlcheck = "SELECT name FROM teams WHERE name = '$teamname'";

    if(!$db->query($sqlcheck)) {
        $db->insert("teams", array('name' => $teamname, 'password' => $teampassword));
        return 1;
    }
    return 2;
}

//changepassword changes the admin account password
//It will only change the password if admin password in entered correctly
function changePassword($username,$password, $newpassword)
{
    $db = new Database();
    $username = addslashes($username);
    $password = addslashes($password);
    $newpassword = addslashes(password_hash($newpassword, PASSWORD_DEFAULT));
    if(checkAdminCredentials($username, $password) == TRUE){
      $db->update("users", array("password" => $newpassword), "username = '$username'" );
      return true;
    } else {
      return false;
	}
	 mysqli_close($dbconnection);
}

//populateTeamList creates a drop down menu of all the teams in the database
function populateTeamList()
{
	$db = new Database();
	$query = "SELECT name FROM teams";
	echo '<select teams="DROP DOWN TEAMS">';

	foreach ($db->query($query) as $row) {
		echo "<option value='" . $row['name'] ."'>" . $row['name'] ."</option>";
	}
	echo '</select>';
}

//countTeams counts the created teams
function countTeams()
{
	$db = new Database();
	$query = "SELECT count(*) AS total FROM teams";
	$result = $db->query($query);
	$team_count = $result ? $result[0]["total"] : 0;
	return $team_count;
}

function countGames(){
    $db = new Database();
	$query = "SELECT count(*) AS total FROM game";
	$result = $db->query($query);
	$team_count = $result ? $result[0]["total"] : 0;
	return $team_count;
}
function printTeamData()
{
	$db = new Database();
	$res = $db->query("SELECT * FROM teams", false);
    return $res;
}
function deleteTeam($id){
    $db = new Database();
	$id = (int) $id;
	$db->delete("teams", "tid = $id");
    return 1;
}

function deleteGame($id){
    $db = new Database();
	$id = (int) $id;
	$db->delete("game", "id = $id");
    return 1;
}

function printGameData()
{
    $db = new Database();
    $res = $db->query("SELECT * FROM game", false);
    return $res;
}

function getTeamName($id)
{
    $db = new Database();
    $res = $db->query("SELECT name FROM teams WHERE tid = ".$id."", false);
    return $res;
}

function gameStatus($id) {
    $db = new Database();
    $res = $db->query("SELECT * FROM game WHERE id = ".$id."", false);
    $arrTeam = explode(",", $res[0]['team_ids']);

    $arrTeamStatus = array();
    $arrTeamStatus['gamename'] = $res[0]['gamename'];
    foreach ($arrTeam as $key => $value) {
        $sqlTeamStatus = $db->query("SELECT * FROM gameStorage WHERE team_id='$value'", false);
        $team = $db->query("SELECT * FROM teams WHERE tid = ".$value."", false);
        $answer_ids = $sqlTeamStatus[0]['answers_id'];
        $teamName = $team[0]['name'];
        $arrTeamStatus[$teamName] = $sqlTeamStatus[0];
        if($answer_ids != null) {
            $answer_ids = substr($answer_ids, 0, -1);
            $arrAnswer_ids = explode(",", $answer_ids);
            $arrAnswer = $db->query("SELECT * FROM answers WHERE team_id = ".$value." ORDER BY question_id", false);
            foreach ($arrAnswer as $keys => $val) {
                $question_id = $val['question_id'];
                $question = $db->query("SELECT * FROM questions WHERE q_id = ".$question_id."", false);
                $arrTeamStatus[$teamName]["p".($keys+1)] = $val;
                $arrTeamStatus[$teamName]["p".($keys+1)]['question'] = $question[0]['question'];
                $arrTeamStatus[$teamName]["p".($keys+1)]['q_answer'] = $question[0]['answer'];
            }
        }

    }
    $dunno = $team[0]['destination_ids'];
    $dunno = explode(",", $dunno);
    $arrTeamStatus['totalCords'] = sizeof($dunno);
    return $arrTeamStatus;
}
?>
