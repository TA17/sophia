<pre>
<?php
session_start();
if (!isset($_SESSION['game']['members'])) {
    $arrMembers = $_POST['group'];
    $strMembers = "";
    foreach ($arrMembers as $key => $value) {
        if ($value != "") {
            $strMembers .= $value.", ";
        }

    }
    $strMembers = rtrim($strMembers, ", ");
    require 'functions.php';
    $team_id = $_SESSION['game']['team_id'];
    if(addmembers($team_id, $strMembers))  {
        $_SESSION['game']['members'] = $strMembers;
        $_SESSION['game'] = true;
    }
    header("Location: ../game/game.php");
}

header("Location: ../game/game.php");
