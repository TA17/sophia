<?php
session_start();
error_reporting(E_ALL);
ini_set('display_errors', '1');

require 'functions.php';

$result = checkTeamCredentials($_POST["name"], $_POST["password"]);
if($result){
    foreach ($result as $key => $value) {
        $_SESSION['game']['team_id'] = $value['tid'];
        $_SESSION['game']['name'] = $value['name'];
        if ($value['game']['members'] == "") {
            header("Location: ../addmembers.php");
        } else {
        $_SESSION['game']['members'] = $value['members'];
        header("Location: ../game/game.php?signup=success");

        }

    }
} else {
    echo "wrong password/teamname";
}
