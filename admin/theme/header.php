<!DOCTYPE html>
<html lang="en">

<head>
    <base href="<?php echo 'https://'.$_SERVER['HTTP_HOST'] . $ScriptKaust; ?>">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Sophia KP</title>

    <!-- Custom fonts for this template-->
    <link href="theme/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="theme/vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
    <link href="theme/css/sb-admin-2.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
    <link rel="stylesheet" href="theme/css/main.css">
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.5.1/dist/leaflet.css"/>
    <link rel="stylesheet" href="theme/map/leaflet.draw.css" />
</head>

<body id="page-top">
    <?php
    if (isset($_SESSION['error']) && !empty($_SESSION['error']['alert'])) {
        if ($_SESSION['error']['success']) {
            ?>
            <p class="toastr toastr-success" style="display:none"><?= $_SESSION['error']['alert']; ?></p>
            <?php
            unset($_SESSION['error']);
        } else {
            ?>
            <p class="toastr toastr-error" style="display:none"><?= $_SESSION['error']['alert']; ?></p>
            <?php
            unset($_SESSION['error']);
        }
    }
    ?>
    <!-- Page Wrapper -->
    <div id="wrapper">
