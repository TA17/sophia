<?php
echo "Tiimide kontroll tuleb siis, kui Kõik tiimid on ära mänginud";

 ?>
 <!-- DataTales Example -->
 <div class="card shadow mb-4">
   <div class="card-header py-3">
     <h6 class="m-0 font-weight-bold text-primary">Kontroll</h6>
   </div>
   <div class="card-body">
     <div class="table-responsive">
       <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
         <thead>
           <tr>
               <th>Punk</th>
               <th>Küsimus</th>
               <th>Vastus</th>
               <th>Mängija Vastus</th>
               <th>Õige</th>
               <th>Vale</th>
           </tr>
         </thead>
         <tfoot>
           <tr>
               <th>Punk</th>
               <th>Küsimus</th>
               <th>Vastus</th>
               <th>Mängija Vastus</th>
               <th>Õige</th>
               <th>Vale</th>
           </tr>
         </tfoot>
         <tbody>
<?php $cords = 1;
while ($totalCords >= $cords): ?>
    <tr>
        <td>Punkt <?php echo ($cords); ?></td>
        <?php if (array_key_exists("p".$cords, $teamStatus['Lumineo'])): ?>
            <td><?php echo $teamStatus['Lumineo']["p".$cords]['question']; ?></td>
            <td><?php echo $teamStatus['Lumineo']["p".$cords]['q_answer']; ?></td>
            <td><?php echo $teamStatus['Lumineo']["p".$cords]['answer']; ?></td>
            <fieldset class="Lumineo<?php echo $key; ?>">
                <td>
                    <div class="form-check form-check-inline">
                      <input class="form-check-input" type="radio" name="q<?php echo $teamStatus['Lumineo']["p".$cords]['question_id']; ?>" id="inlineRadio1" value="option1">
                    </div>

                </td>
                <td>
                    <div class="form-check form-check-inline">
                      <input class="form-check-input" type="radio" name="q<?php echo $teamStatus['Lumineo']["p".$cords]['question_id']; ?>" id="inlineRadio2" value="option2">
                    </div>
                </td>
            </fieldset>

        <?php else: ?>
            <td>NaN</td>
            <td>NaN</td>
            <td>NaN</td>
            <td>NaN</td>
            <td>NaN</td>
        <?php endif; ?>

    </tr>
<?php $cords++; endwhile; ?>

         </tbody>
       </table>
     </div>
   </div>
 </div>
 <pre>
     <?php //print_r(gameStatus($id)); ?>
     <?php print_r($teamStatus['Lumineo']); ?>
 </pre>
