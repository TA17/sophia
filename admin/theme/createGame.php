<!-- Begin Page Content -->
<div class="container-fluid">
    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800">Mängu loomine</h1>
    <div class="progress">
        <div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
    </div>
    <form class="" action="index.html" method="post">
        <div id="page1" class=" mt-2">
            <div class="form-group">
                <label for="inputGameName">Mängu nimi</label>
                <input type="text" class="form-control" name="game_name" id="inputGameName" placeholder="Mängu nimi">
            </div>
        </div>
        <div id="page2" class=" mt-2" style="display:none">
            <h2>Vali tiimid</h2>
            <?php
            $count = 0;
              $teams = printTeamData();
              if($teams){
                  foreach($teams as $key => $row){ ?>
                      <?php if ($row['destination_ids'] == null): $count++;?>


            <div class="form-check">
                <input class="form-check-input" name="team_id" type="checkbox" value="<?php echo $row['tid']; ?>" id="defaultCheck<?php echo $row['tid']; ?>">
                <label class="form-check-label" for="defaultCheck<?php echo $row['tid']; ?>">
                    <?php echo $row['name']; ?>
                </label>
            </div>
        <?php  endif;  }
              } ?>
              <?php if ($count == 0): ?>
                  <h2 class="text-danger">Sa pead tiime lisama <br> Tiimid mis on juba mängus ei saa teise mängu panna!</h2>
              <?php endif; ?>
        </div>
        <div id="page3" class=" mt-2" style="display:none">
            <div class="row">
                <div class="col-3 cords">
                </div>

                <div class="col-9">
                        <div id="formMap" style="max-height: 75%;"></div>
                        <div id="saveLayer"  style="display:none">
                            <button type="button" id="save" class="btn btn-primary float-right mt-3" name="button">Salvesta</button>
                        </div>
                </div>
            </div>
            <div class="map"></div>
        </div>
        <div id="switch">
            <div id="nextLayer" >
                <button type="button" id="next" class="btn btn-primary float-right" name="button">Järgmine</button>
            </div>
            <div id="backLayer">

            </div>
        </div>
    </form>
</div>
<!-- /.container-fluid -->
<!-- Modal -->
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Küsimuse sisestamine</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <div class="form-group">
              <label for="question">Küsimus</label>
              <input type="text" name="question" id="question" class="form-control" placeholder="küsimus" value="">
          </div>
          <div class="">
              <label for="anwser">Vastus</label>
              <input type="text" name="answer" id="answer" class="form-control" placeholder="vastus" value="">
          </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" id="cordsSave" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>
