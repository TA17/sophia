
<!-- Begin Page Content -->
<div class="container-fluid">
    <?php
    $url = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
    //echo $url;
    $arrURL = explode('/', $url);
    $id = end($arrURL);
    if (ctype_digit($id)):

        $teamStatus = gameStatus($id);
        ?>
        <h2><?php echo "Mäng - ".$teamStatus['gamename']; ?></h2>
        <div class="">
            <button type="button" class="btn btn-white shadow text-center font-weight-bold text-primary text-uppercase mb-1 mr-5" name="status">Staatus</button>
            <button type="button" class="btn btn-white shadow text-center font-weight-bold text-primary text-uppercase mb-1 mr-5" name="check">Kontroll</button>
            <button type="button" class="btn btn-white shadow text-center font-weight-bold text-primary text-uppercase mb-1 mr-5" name="result">Tulemus</button>
        </div>
        <hr class="mb-5">
        <div id="status">
            <?php include 'gameTeam.php'; ?>
        </div>
        <div id="controlCheck" style="display: none;">
            <?php include 'gameControl.php'; ?>
        </div>
        <div id="endGame" style="display: none;">
            <?php include 'gameEnd.php' ?>
        </div>
        <?php
    else:
     ?>
    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800">Lõpp</h1>
    <!-- Content Row -->
    <div class="row">

<?php foreach ($games = printGameData() as $key => $value):?>
        <div class="col-xl-3 col-md-6 mb-4">
            <div class="card shadow h-100 py-2"><a style=" text-decoration: none" href="gameFinal/<?php echo $value['id']; ?>">
                <div class="card-body">
                    <div class=" no-gutters align-items-center">
                        <div class="text-center font-weight-bold text-primary text-uppercase mb-1"><?php echo $value['gamename'];; ?></div>
                    </div>
                </div>
            </div></a>
        </div>
<?php endforeach; ?>
    <!-- Content Row -->
</div>
<!-- /.container-fluid -->
<?php
endif;
 ?>
</div>
