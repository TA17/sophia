<!-- Begin Page Content -->
<div class="container-fluid">
    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800">Mängude nimekiri</h1>

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
      <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Mängude nimekiri</h6>
      </div>
      <div class="card-body">
        <div class="table-responsive">
          <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Mängu nimi</th>
                    <th>Meeskonnad</th>
                    <th></th>
                </tr>
            </thead>
            <tfoot>
                <tr>
                    <th>#</th>
                    <th>Mängu nimi</th>
                    <th>Meeskonnad</th>
                    <th></th>
                </tr>
            </tfoot>
            <tbody>
                <?php
                $games = printGameData();
                if($games){
                    foreach($games as $key => $row){
                        $gameTeams = explode(",", $row['team_ids']);
                        $teamsList = "";
                        foreach($gameTeams as $index) {
                            $teamsList .= getTeamName($index)[0]['name'].", ";
                        }
                        $teamsList = rtrim($teamsList, ", ");
                        ?>
                        <tr>
                            <td><?php echo $key+1?></td>
                            <td><?php echo $row['gamename']; ?></td>
                            <td><?php echo $teamsList; ?></td>
                            <form class="" action="process.php" method="post">
                                <td>
                                    <input type="hidden" name="delete_id" value="<?php echo $row['id']; ?>">
                                    <input type="submit" name="deleteGame" value="Kustuta" class="btn btn-danger">
                                </td>
                            </form>
                        </tr>
                        <?php
                    }
                }
                 ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
</div>
<!-- /.container-fluid -->
