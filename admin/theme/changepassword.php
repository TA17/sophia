<div class="col-lg-8 col-md-7">

    <div class="card shadow">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Adminikasutaja parooli muutmine</h6>
        </div>
        <div class="content">
            <form action="process.php" method="post">
                <div class="row mt-3">
                    <div class="col-md-3 ml-5">
                        <div class="form-group">
                            <label>Nimi</label>
                            <input type="text" class="form-control border-input" name="username" placeholder="Admini kasutaja nimi" required>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Vana Parool</label>
                            <input type="password" class="form-control border-input" name="password" placeholder="Admini vana parool" required>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Uus parool</label>
                            <input type="password" class="form-control border-input" name="new_password" placeholder="Admini uus parool" required>
                        </div>
                    </div>
                </div>
                <div class="text-center">
                    <button type="submit" name="changePassword" value= "Muuda" class="btn btn-primary btn-fill btn-wd mb-4">Vaheta parool</button>
                </div>
            </form>
        </div>
    </div>
</div>
