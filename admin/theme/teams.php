<!-- Begin Page Content -->
<div class="container-fluid">
    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800">Tiimide nimekiri</h1>

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
      <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Tiimide nimekiri</h6>
      </div>
      <div class="card-body">
        <div class="table-responsive">
          <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Tiimi nimi</th>
                    <th>Tiimi parool</th>
                    <th>Tiimide liikmed</th>
                    <th></th>
                </tr>
            </thead>
            <tfoot>
                <tr>
                    <th>#</th>
                    <th>Tiimi nimi</th>
                    <th>Tiimi parool</th>
                    <th>Tiimide liikmed</th>
                    <th></th>
                </tr>
            </tfoot>
            <tbody>
                <?php
                $teams = printTeamData();
                if($teams){
                    foreach($teams as $key => $row){
                        if ($row['members'] == "") {
                            $row['members'] = "Tiim ei ole liikmeid sisestanud";
                        }
                        ?>
                        <tr>
                            <td><?php echo $key+1?></td>
                            <td><?php echo $row['name']; ?></td>
                            <td><?php echo $row['password']; ?></td>
                            <td><?php echo $row['members']; ?></td>
                            <form class="" action="process.php" method="post">
                                <td>
                                    <input type="hidden" name="delete_id" value="<?php echo $row['tid']; ?>">
                                    <input type="submit" name="deleteTeam" value="Kustuta" class="btn btn-danger">
                                </td>
                            </form>
                        </tr>
                        <?php
                    }
                }
                 ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
</div>
<!-- /.container-fluid -->
