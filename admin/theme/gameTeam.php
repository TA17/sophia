<?php
$row = "";
$tableStart = true;

array_shift($teamStatus);
$counter = 1;
$cords = 1;
$totalCords = $teamStatus['totalCords'];
unset($teamStatus['totalCords']);
foreach ($teamStatus as $key => $value):
    if ($tableStart) {
        ?>
        <table class="table">
            <thead>
                <tr>
                    <th>Tiimin nimi</th>

                    <?php while($totalCords >= $cords ): ?>
                        <th><?php echo "punkt {$cords}"; ?></th>
                    <?php $cords++; endwhile;  ?>
                    <th>Staatus</th>
                </tr>

            </thead>
            <tbody>
        <?php
        $cords = 1;
        $tableStart = false;
    }
    ?>
    <tr>
        <td><?php echo $key; ?></td>

        <?php while($totalCords >= $cords ): ?>
            <?php if (!is_null($teamStatus[$key])): ?>
                <?php if (array_key_exists("p".$cords, $teamStatus[$key])): ?>
                    <td>Läbitud</td>
                <?php else: ?>
                    <td>Läbimata</td>
                <?php endif; ?>
            <?php else: ?>
                <td>Läbimata</td>
            <?php endif; ?>
        <?php $cords++; endwhile; $cords =1;
        if(array_key_exists($key, $teamStatus) && is_null($teamStatus[$key])) {
            ?><td>Ei ole alustanud </td><?php
        } elseif (!is_null($teamStatus[$key]['end'])){
            ?><td>Lõpetanud</td><?php
        } else {
            ?><td>Mängib</td><?php
        }

        ?>

    </tr>
    <?php
    $counter++;
endforeach;
  ?>


      </tbody>
  </table>
  <pre>
  <?php
  //print_r($teamStatus);
  ?></pre><?php
  ?>
