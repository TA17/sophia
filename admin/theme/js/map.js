function map() {
    var layer;
    var question;
    var answer;
    var markerLayer;
    var arrMarkers = new Array();
    var tempMarker;
    var marker;
    var startFin = new Array();
    var lat;
    var map = L.map('formMap').setView([51.505, -0.09], 13);
    L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
        attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
        maxZoom: 18,
        id: 'mapbox.streets',
        accessToken: 'pk.eyJ1IjoidGFhdmlvIiwiYSI6ImNqcW5zZzA5MDA2M3o0M281c2FoNWNxN2gifQ.5OYQtCfZHKZ471A_bzuisw',
    }).addTo(map);
    // FeatureGroup is to store editable layers
    var drawnItems = new L.FeatureGroup();
    map.addLayer(drawnItems);
    var drawControl = new L.Control.Draw({
        draw: {
            polyline: false,
            circlemarker: false,
            circle: false,
            rectangle: false,
        },
        edit: {
            featureGroup: drawnItems
        }
    });
    map.addControl(drawControl);
    map.on(L.Draw.Event.CREATED, function(e) {
        question = null;
        answer = null;
        var type = e.layerType;
        var layer = e.layer;
        markerLayer = e.layer;
        if (type == 'marker') {
            tempMarker = layer._latlng;
            lat = tempMarker.lat;
            lng = tempMarker.lng;
            $('#exampleModalCenter').modal('show');
            $('#cordsSave').on('click', function(e) {
                question = null;
                answer = null;
                question = $("#exampleModalCenter #question").val().trim()
                answer = $("#exampleModalCenter #answer").val().trim()
                if (question.length != 0 && answer.length != 0) {
                    drawnItems.addLayer(markerLayer);
                    var div = "<div id=\"" + markerLayer._leaflet_id + "\" class=\"cord-box  mb-4\">"
                    div += '<p class="latFileld">lat: ' + lat + "</p>";
                    div += '<p class="lngFileld">lng: ' + lng + "</p>";
                    div += '<p class="questionField">Küsimus: ' + question + "</p>";
                    div += '<p class="answerFileld">Vastus: ' + answer + "</p>";
                    div += '</div>';
                    $(div).appendTo('.cords');
                }
                console.log("q: " + question);
                console.log("a: " + answer.length);
                $("#exampleModalCenter #question").val("");
                $("#exampleModalCenter #answer").val("");
                question = null;
                answer = null;
                $('#exampleModalCenter').modal('hide');
            });
            $('#cordsQuit').on('click', function(e) {
                drawnItems.removeLayer(markerLayer);
                $('#exampleModalCenter').modal('hide');
            });
        }
        if (type === 'polygon') {
            tempMarker = layer._latlngs[0];
            $.each(tempMarker, function(key, value) {
                console.log(value.lng);
                console.log(".....");
                startFin.push({
                    'lat': value.lat,
                    'lng': value.lng
                });
            });
            console.log(startFin);
            drawnItems.addLayer(layer);
            drawControl.setDrawingOptions({
                polygon: false
            });
            map.removeControl(drawControl);
            map.addControl(drawControl);
            /*lat = tempMarker.lat;
            lng = tempMarker.lng;
            var question = $("#exampleModalCenter #question").val().trim()
            var answer = $("#exampleModalCenter #answer").val().trim()

            $(div).appendTo('.cords');
            dataid++;*/
            drawnItems.addLayer(layer);

        }
        question = null;
        answer = null;
    });
    map.on('draw:edited', function(e) {
        var layers = e.layers;
        layers.eachLayer(function(a) {
            layerid = a._leaflet_id;
            latlng = a._latlng;
            var editLat = latlng.lat;
            var editLng = latlng.lng;
            $('#' + layerid).find('.latFileld').text('lat: ' + editLat);
            $('#' + layerid).find('.lngFileld').text('lng: ' + editLng);
            console.log(layerid);
            console.log(editLat);
            console.log(editLng);

        });
    });

    map.on("draw:deleted", function(e) {
        var layers = e.layers;

        console.log(layers);
        layers.eachLayer(function(layer) {
            if (layer.editing._poly) {
                startFin = null;
                drawControl.setDrawingOptions({
                    polygon: true
                });
            } else {
                var plat = layer._latlng.lat;
                //$('div').has('div:contains(" + plat + ")').remove()
                $('p:contains(' + plat + ')').closest('div').remove();
                //$("div:contains(" + plat + ")").first().remove()
            }
        });


        map.removeControl(drawControl);
        map.addControl(drawControl);
    });
    $('#save').on('click', function() {
        var i = 0;
        $('.cord-box').each(function() {
            // iterate through each of the `.target` elements, and do stuff in here
            // // `this` and `$(this)` refer to the current `.target` element
            var fieldLat = $(this).find('.latFileld').text().replace('lat: ', '');
            var fieldLng = $(this).find('.lngFileld').text().replace('lng: ', '');
            var fieldQuestion = $(this).find('.questionField').text().replace('Küsimus: ', '');
            var fieldAnswer = $(this).find('.answerFileld').text().replace('Vastus: ', '');

            /*console.log("------");
            console.log(fieldLat);
            console.log(fieldLng);
            console.log(fieldQuestion);
            console.log(fieldAnswer);*/
            arrMarkers[i] = new Array();
            arrMarkers[i].push(fieldLat);
            arrMarkers[i].push(fieldLng);
            arrMarkers[i].push(fieldQuestion);
            arrMarkers[i].push(fieldAnswer);
            i++;
        });
        console.log(arrMarkers);
        console.log(startFin);
        var gameName = $('#inputGameName').val();
        console.log("game name: " + gameName);
        var selected = [];
        $('#page2 input:checked').each(function() {
            selected.push($(this).attr('value'));
        });
        if ($.isEmptyObject(startFin) || $.isEmptyObject(arrMarkers)) {
            var str = $( ".toastr-error" ).text();
            toastr.error("Sul peavad olema punktid ja ALA kus nad alustavad JA lõpetavad");
        } else {
            $.ajax({
                    url: 'https://sophia.lunar.icu/admin/ajax/Game.php',
                    type: 'POST',
                    data: {
                        'markers': arrMarkers,
                        'startfin': startFin,
                        'teams': selected,
                        'gameName': gameName
                    }, //, 'teams': selected, 'gameName': gameName
                    async: true,
                    beforeSend: function() {
                        console.log("loading");
                    },
                    complete: function() {
                        location.assign("https://sophia.lunar.icu/admin/gameFinal");
                        console.log("Done with this shit");
                        //$('#loading_wrap').hide();
                    },
                })
                .done(function() {
                    console.log("success");
                    //
                })
                .fail(function() {
                    console.log("error");
                });
        }
    });
    map.invalidateSize();

    function clearMarkers() {
        markers.clearLayers();
    }

    function cleanStringify(object) {
        if (object && typeof object === 'object') {
            object = copyWithoutCircularReferences([object], object);
        }
        return JSON.stringify(object);

        function copyWithoutCircularReferences(references, object) {
            var cleanObject = {};
            Object.keys(object).forEach(function(key) {
                var value = object[key];
                if (value && typeof value === 'object') {
                    if (references.indexOf(value) < 0) {
                        references.push(value);
                        cleanObject[key] = copyWithoutCircularReferences(references, value);
                        references.pop();
                    } else {
                        cleanObject[key] = '###_Circular_###';
                    }
                } else if (typeof value !== 'function') {
                    cleanObject[key] = value;
                }
            });
            return cleanObject;
        }
    }
}

/*function map() {
    var dataid = 200;
    var lat;
    var markers;
    var arrMarkers = new Array();
    var lng;
    var marker;
    var map = L.map('formMap').setView([58.926415, 24.872596], 13);
    L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
        attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
        maxZoom: 18,
        id: 'mapbox.streets',
        accessToken: 'pk.eyJ1IjoidGFhdmlvIiwiYSI6ImNqcW5zZzA5MDA2M3o0M281c2FoNWNxN2gifQ.5OYQtCfZHKZ471A_bzuisw',
    }).addTo(map);
    var popup = L.popup();
    function onMapClick(e) {
        var question = "";
        var answer = "";
        popup
            .setLatLng(e.latlng)
            .setContent(e.latlng.toString())
            .openOn(map);
        var latlng = e.latlng;
        console.log(latlng);
        lat = e.latlng.lat;
        lng = e.latlng.lng;
        $('#exampleModalCenter').modal('show')
        $('#cordsSave').on('click', function(e) {
            var question = $("#exampleModalCenter #question").val().trim()
            var answer = $("#exampleModalCenter #answer").val().trim()
            if (question.length != 0 && answer.length  != 0) {
                    var div = "<div class=\"cord-box  mb-4\">"
                    div += '<p id="latFileld">lat: ' + lat + "</p>";
                    div += '<p id="lngFileld">lng: ' + lng + "</p>";
                    div += '<p id="questionField">Küsimus: ' + question + "</p>";
                    div += '<p id="answerFileld">Vastus: ' + answer + "</p>";
                    div += '</div>';
                    marker = L.marker([lat,lng]).addTo(map);
                    marker.bindPopup("Olen kohal").openPopup();
                    //$('.cords').prepend($(div));
                    $(div).appendTo('.cords');
                    dataid++;
            }
        $("#exampleModalCenter #question").val("");
        $("#exampleModalCenter #answer").val("");
        $('#exampleModalCenter').modal('hide');
        });
    }
    $('#save').on('click', function() {
        var i =0;
        $('.cord-box').each(
            function(){
                // iterate through each of the `.target` elements, and do stuff in here
                // // `this` and `$(this)` refer to the current `.target` element
                var fieldLat = $(this).find('#latFileld').text().replace('lat: ', '');
                var fieldLng = $(this).find('#lngFileld').text().replace('lng: ', '');
                var fieldQuestion = $(this).find('#questionField').text().replace('Küsimus: ', '');
                var fieldAnswer = $(this).find('#answerFileld').text().replace('Vastus: ', '');

                console.log("------");
                console.log(fieldLat);
                console.log(fieldLng);
                console.log(fieldQuestion);
                console.log(fieldAnswer);
                arrMarkers[i] = new Array();
                arrMarkers[i].push(fieldLat);
                arrMarkers[i].push(fieldLng);
                arrMarkers[i].push(fieldQuestion);
                arrMarkers[i].push(fieldAnswer);
                i++;
            });
            var gameName = $('#inputGameName').val();
            console.log("game name: " + gameName);
            var selected = [];
            $('#page2 input:checked').each(function() {
                selected.push($(this).attr('value'));
            });
            $.ajax({
                url: 'https://sophia.lunar.icu/admin/ajax/Game.php',
                type: 'POST',
                data: {'markers': arrMarkers, 'teams': selected, 'gameName': gameName}
            })
            .done(function() {
                console.log("success");
            })
            .fail(function() {
                console.log("error");
            })
            .always(function() {
                console.log("complete");
            });

    });
    map.invalidateSize();
    map.on('click', onMapClick);
    function clearMarkers() {
        markers.clearLayers();
    }
}
*/
