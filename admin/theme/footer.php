</div>
  <!-- End of Main Content -->
<!-- Footer -->
<footer class="sticky-footer bg-white">
  <div class="container my-auto">
    <div class="copyright text-center my-auto">
      <span>Copyright &copy; LunarTech <?php echo date("Y"); ?></span>
    </div>
  </div>
</footer>
<!-- End of Footer -->

</div>
<!-- End of Content Wrapper -->

</div>
<!-- End of Page Wrapper -->

<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
<i class="fas fa-angle-up"></i>
</a>

<!-- Logout Modal-->
<div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
<div class="modal-dialog" role="document">
<div class="modal-content">
  <div class="modal-header">
    <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
      <span aria-hidden="true">×</span>
    </button>
  </div>
  <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
  <div class="modal-footer">
    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
    <a class="btn btn-primary" href="login.html">Logout</a>
  </div>
</div>
</div>
</div>

<!-- Bootstrap core JavaScript-->
<script src="theme/vendor/jquery/jquery.min.js"></script>
<script src="theme/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js" charset="utf-8"></script>
<!-- Core plugin JavaScript-->
<script src="theme/vendor/jquery-easing/jquery.easing.min.js"></script>
<script src="theme/js/sb-admin-2.min.js"></script>
<!-- Page level plugins -->
<script src="theme/vendor/datatables/jquery.dataTables.min.js"></script>
<script src="theme/vendor/datatables/dataTables.bootstrap4.min.js"></script>
<script src="theme/js/demo/datatables-demo.js"></script>

<script src="https://unpkg.com/leaflet@1.5.1/dist/leaflet.js"></script>
<script src="theme/map/Leaflet.draw.js"></script>
<script src="theme/map/Leaflet.Draw.Event.js"></script>
<script src="theme/map/Toolbar.js"></script>
<script src="theme/map/Tooltip.js"></script>

<script src="theme/map/ext/GeometryUtil.js"></script>
<script src="theme/map/ext/LatLngUtil.js"></script>
<script src="theme/map/ext/LineUtil.Intersect.js"></script>
<script src="theme/map/ext/Polygon.Intersect.js"></script>
<script src="theme/map/ext/Polyline.Intersect.js"></script>
<script src="theme/map/ext/TouchEvents.js"></script>

<script src="theme/map/draw/DrawToolbar.js"></script>
<script src="theme/map/draw/handler/Draw.Feature.js"></script>
<script src="theme/map/draw/handler/Draw.SimpleShape.js"></script>
<script src="theme/map/draw/handler/Draw.Polyline.js"></script>
<script src="theme/map/draw/handler/Draw.Marker.js"></script>
<script src="theme/map/draw/handler/Draw.Circle.js"></script>
<script src="theme/map/draw/handler/Draw.CircleMarker.js"></script>
<script src="theme/map/draw/handler/Draw.Polygon.js"></script>
<script src="theme/map/draw/handler/Draw.Rectangle.js"></script>


<script src="theme/map/edit/EditToolbar.js"></script>
<script src="theme/map/edit/handler/EditToolbar.Edit.js"></script>
<script src="theme/map/edit/handler/EditToolbar.Delete.js"></script>

<script src="theme/map/Control.Draw.js"></script>

<script src="theme/map/edit/handler/Edit.Poly.js"></script>
<script src="theme/map/edit/handler/Edit.SimpleShape.js"></script>
<script src="theme/map/edit/handler/Edit.Rectangle.js"></script>
<script src="theme/map/edit/handler/Edit.Marker.js"></script>
<script src="theme/map/edit/handler/Edit.CircleMarker.js"></script>
<script src="theme/map/edit/handler/Edit.Circle.js"></script>
<script src="theme/js/map.js" charset="utf-8"></script>
<!-- Custom scripts for all pages-->
<script type="text/javascript">
$(document).ready(function(){
    if ($(".toastr")[0]) {
        toastr.options.progressBar = true;
        toastr.options.preventDuplicates = false;
        toastr.options.closeButton = true;

        if ($(".toastr-warning")[0]) {
            var str = $( ".toastr-warning" ).text();
            toastr.warning(str);
        } else if ($(".toastr-success")[0]) {
            var str = $( ".toastr-success" ).text();
            toastr.success(str);
        } else if ($(".toastr-info")[0]) {
            var str = $( ".toastr-info" ).text();
            toastr.info(str);
        } else if ($(".toastr-error")[0]) {
            var str = $( ".toastr-error" ).text();
            toastr.error(str);
        }
    }
});
var progress = 100 / 3;
var page = 1;
var add = 100 / 3;
var progresscss;
$("#next").on('click', function() {
    if (page == 1) {
        if ($('#inputGameName').val().length != 0) {
            $('#page' + page).hide();
            page++;
            $('#page' + page).show();
            progresscss = progress + "%";

        } else {
            $('#inputGameName').addClass('is-invalid');
        }
    } else if (page == 2) {
        var selected = [];
        $('#page2 input:checked').each(function() {
            selected.push($(this).attr('value'));
        });
        console.log(selected);
        if (selected.length <= 1) {
            var str = $( ".toastr-error" ).text();
            toastr.error("Sa pead valima vähemalt 2 tiimi!");
        } else {
            $('#page' + page).hide();
            page++;
            $('#page' + page).show();
            progress = progress + add;
            progresscss = progress + "%";
        }
    } else {

    }



    console.log(progresscss);
    $('.progress-bar').css('width', progresscss);
    //progress = progress + add;
    if (page == 3) {
        $('#nextLayer').hide();
        $('#saveLayer').show();
        map();
    }
});
$(function() {
    $("button[name='status']").on('click', function(event) {
        event.preventDefault();
        $( "#status" ).css('display', 'block');
        $( "#controlCheck" ).css('display', 'none');
        $( "#endGame" ).css('display', 'none');
    });
    $("button[name='check']").on('click', function(event) {
        event.preventDefault();
        $( "#status" ).css('display', 'none');
        $( "#controlCheck" ).css('display', 'block');
        $( "#endGame" ).css('display', 'none');
    });
    $("button[name='result']").on('click', function(event) {
        event.preventDefault();
        $( "#status" ).css('display', 'none');
        $( "#controlCheck" ).css('display', 'none');
        $( "#endGame" ).css('display', 'block');
    });
})
</script>
</body>

</html>
