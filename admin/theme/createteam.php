<!-- Begin Page Content -->
<div class="container-fluid">
    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800">Tiimide loomine</h1>
    <form action="process.php" method="post">
        <div class="form-row">
            <div class="form-group col-md-6">
                <label for="inputEmail4">Tiimi nimi</label>
                <input type="text" class="form-control" name="team_name" id="inputEmail4" placeholder="Tiimi nimi">
            </div>
            <div class="form-group col-md-6">
                <label for="inputPassword4">Tiimi parool</label>
                <input type="password" class="form-control" name="password" id="inputPassword4" placeholder="Tiimi parool">
            </div>
        </div>
        <button type="submit" value= "Loo" name="createTeam" class="btn btn-info btn-fill btn-wd">Loo tiim</button>
    </form>
</div>
<!-- /.container-fluid -->
