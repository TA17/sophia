<?php
require '../inc/functions.php';
session_start();
class Process
{
    function process() {
        $arrPost = $_POST;
        end($arrPost);
        print_r($_POST);
        $post = key($arrPost);
        if(isset($post) || $post != null) {
            $this->$post();
        } else {
            $this->logout();
        }
    }

    function login() {
        $username = $_POST['username'];
        $password = $_POST['password'];
        $isCorrect = checkAdminCredentials($username,$password);
        //print_r($isCorrect['0']['password']);
        if($isCorrect == true) {
            echo "käisin siin ka";
            $_SESSION["islogin"] = true;
            $_SESSION['error']['alert'] = "Logisid edukalt sisse";
            $_SESSION['error']['success'] = true;
            header("Location: ".$_SERVER["HTTP_REFERER"]);
        }
    }
    function logout() {
        $_SESSION = array();
        header("Location: login.php");
    }

    function createTeam() {
        $result = createTeam($_POST['team_name'],$_POST['password']);
        switch ($result) {
            case 1:
                $_SESSION['error']['alert'] = "tiim on edukalt loodud";
                $_SESSION['error']['success'] = true;
                header("Location: ".$_SERVER["HTTP_REFERER"]);
                break;
            default:
                $_SESSION['error']['alert'] = "Tiim on juba olemas!";
                $_SESSION['error']['success'] = false;
                header("Location: ".$_SERVER["HTTP_REFERER"]);
                break;
        }
    }
    function deleteTeam() {
        $delete_id =  $_POST['delete_id'];
        $result = deleteTeam($delete_id);
        if($result) {
            $_SESSION['error']['alert'] = "tiim on edukalt kustutatud";
            $_SESSION['error']['success'] = true;
            header("Location: ".$_SERVER["HTTP_REFERER"]);
        }
    }

    function deleteGame() {
        $delete_id =  $_POST['delete_id'];
        $result = deleteGame($delete_id);
        if($result) {
            $_SESSION['error']['alert'] = "tiim on edukalt kustutatud";
            $_SESSION['error']['success'] = true;
            header("Location: ".$_SERVER["HTTP_REFERER"]);
        }
    }

    function changePassword() {
        $username = $_POST['username'];
        $password = $_POST['password'];
        $new_password = $_POST['new_password'];
        $result = changePassword($username, $password, $new_password);
        if($result) {
            $_SESSION['error']['alert'] = "Parool edukalt vahetatud";
            $_SESSION['error']['success'] = true;
            header("Location: ".$_SERVER["HTTP_REFERER"]);
        } else {
            $_SESSION['error']['alert'] = "Parooli vahetamisega tekkis probleem";
            $_SESSION['error']['success'] = false;
            header("Location: ".$_SERVER["HTTP_REFERER"]);
        }
    }
}

$run = new Process();
