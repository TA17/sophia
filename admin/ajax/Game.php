<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
include '../../classes/Database.class.php';
$db = new Database();
$markers = $_POST['markers'];
$startfin = $_POST['startfin'];
$startfin = json_encode($startfin);

$gameName = $_POST['gameName'];
$arrTeams = $_POST['teams'];
$team_ids = "";
$questions_ids = "";
$destination_ids = "";
foreach ($markers as $key => $val) {
    $lon = $val['0'];
    $lng = $val['1'];
    $question = $val['2'];
    $answer = $val['3'];
    echo "$lon"; echo "<br>";
    echo "$lng"; echo "<br>";
    echo "$question"; echo "<br>";
    echo "$answer"; echo "<br>";

    $questions_id = $db->insert("questions", array('answer' => $answer, 'question' => $question));
    $destination_id = $db->insert("destination", array('lat' => $lon, 'lon' => $lng, 'question_id' => $questions_id));
    $destination_ids .= $destination_id.",";
}
 $destination_ids = rtrim($destination_ids, ",");
 $destination_ids = explode(",", $destination_ids);


 foreach ($arrTeams as $key => $val) {
     $team_ids .=  $val.",";
     //print_r($destination_ids);
     $destination_ids = implode(",", $destination_ids);
     $result = $db->update("teams", array('destination_ids' => $destination_ids), "tid = ".$val);
     //print_r($destination_ids);
     $destination_ids = explode(",", $destination_ids);
     $last_key = key($destination_ids);
     $last_value = array_pop($destination_ids);
     $destination_ids = array_merge(array($last_key => $last_value), $destination_ids);
 }
 $team_ids = rtrim($team_ids, ",");




 $result = $db->insert("game", array('gamename' => $gameName, 'team_ids' => $team_ids, 'startfin' => $startfin));
 if ($result) {
     $_SESSION['error']['alert'] = "Mäng on edukalt loodud";
     $_SESSION['error']['success'] = true;
     header("Location: ".$_SERVER["HTTP_REFERER"]);
 }

 ?>
