<?php
session_start();
if(!isset($_SESSION['islogin'])) {
  header("Location: /admin/login.php");
}
require '../inc/functions.php';
$ScriptKaust = "/admin/";
require 'theme/header.php';
#eemaldame kausta osa URL reast
$request = str_replace($ScriptKaust, "/", $_SERVER['REQUEST_URI']);
#eemaldame $request eest / ehk /default_seo/ -> default_seo/
$request = substr($request, 1, strlen($request));
#teeme selle slashist (/) osadeks
$req = explode('/', $request);

require 'theme/navbar.php';
if (!empty($req[0]) and $req[0] != 'index') {
    #seega req[0] on failinimi ilma php-ta
    $file = $req[0].'.php';
    $file = "theme/$file";
    if (file_exists($file) and is_file($file)) {
        #failö on, seega laeme
        require_once($file);
    } else {
        #faili ei leitud. öae emda error või näita infi
        include 'theme/404.php';
    }
} else {
    #see on siis kavaaleht
    include 'theme/index.php';
}
require 'theme/footer.php';
