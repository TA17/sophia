<?php
include '../inc/header.inc.php';
session_start();
if(!isset($_SESSION['game']) && $_SESSION['game'] != true) {
  header("Location: /index.php");
}
//print_r($_SESSION);
require '../inc/functions.php';

?>
<style media="screen">
    .centerPage {
        height: 100%;
        width: 100%;
        display: flex;
        position: fixed;
        align-items: center;
        justify-content: center;
    }
</style>
<div id="game-over" class="centerPage" style="display:none">
    <h1>Mäng läbi :^)</h1>
</div>

<div id="gamemap"></div>
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Küsimus</h5>
      </div>
      <div class="modal-body">
          <label class="form-label" for="question" id="lblq">Kes ma olen?</label>
          <input type="text" class="form-control" id="question" name="" value="">
      </div>
      <div class="modal-footer">
        <button type="button" id="btnQuest" class="btn btn-primary">Vasta</button>
      </div>
    </div>
  </div>
</div>

<?php
$scripts = file_get_contents("../scripts/gameMap.js");


include '../inc/footer.inc.php'; ?>
