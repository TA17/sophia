Sophia funktsionaalsed nõuded:



1) Adminipaneelis tiimide registreerimine - Prioriteet: Keskmine

Sissejuhatus: Admin peab saama luua tiime, koos tiiminime, parooli, ning mänguga, millega nad on seotud.

Sisendid: Tiiminimi, parool, mäng.

Andmete töötlemine: Andmed sisestatakse andmebaasi "TEAMS" tabelisse.

Väljundid: Luuakse tiim.

Veakäitlus: Ühe nimega tiime ei tohi olla.


2) Adminipaneelis tiimide haldamine. - Prioriteet: Keskmine

Sissejuhatus: Admin peab saama hallata tiime, ehk kustutada tiime, lisada ja eemaldada liikmeid. Admin näeb ka nende andmeid, kui mäng on käimas. Ehk kus punktides nad on käinud ja millele nad on õieti vastanud.

Sisendid: Olemasolev tiim, nende andmed.

Andmete töötlemine: Tiim kas kustutakse, või sinna lisatakse või eemaldatakse liikmed andmebaasis.

Väljundid: Uute liikmetega tiim, või siis tiim on kustutatud.

Veakäitlus: Tiimiliikmed peavad eraldatud olema komadega.


3) Adminipaneelis mängude loomine - Prioriteet: Keskmine

Sissejuhatus: Admin peab saama luua mänge, ehk valida selle jaoks sobiv asukoht ja lisada sinna punktid, mida mängijad peavad läbima ja seal vastama küsimustele, mida admin kirjutab.

Sisendid: Mängu asukoht, läbitavad punktid ning küsimused koos vastustega.

Andmete töötlemine: Need andmed lisatakse andmebaasi.

Väljundid: Loodud mäng.

Veakäitlus: Mängudel ei või olla samad nimed. Läbitavad punktid ja küsimused peavad olemas olema.


4) Adminipaneeli sisselogimine. - Prioriteet: Keskmine

Sissejuhatus: Admin peab saama eelloodud kasutajaga adminipaneeli sisse logida.

Sisendid: Olemasolev kasutajanimi ja parool.

Andmete töötlemine: Sisestatud andmed võrreldakse andmebaasis olevate andmetega.

Väljundid: Läbipääs adminpaneeli.

Veakäitlus: Kasutajanimi ja parool peavad olema õiged.


5) Admini andmete muutmine - prioriteet: madal

Sissejuhatus: Kui admin on adminipaneeli sisse logitud, peab ta saama oma sisselogimisandmeid muuta.

Sisendid: Olemasolevad andmed ning uued andmed.

Andmete töötlemine: Esmalt võrreldakse vanasid andmeid, ja siis vahetatakse need uute vastu välja.

Väljundid: Admini kasutaja uus kasutajanimi ja parool.

Veakäitlus: Vanad andmed peavad olema õiged.


 6) Adminipaneelis mängude haldus. - Prioriteet: Keskmine

Sissejuhatus: Admin peab saama muuta olemasolevate mängude kõiki andmeid ning neid kustutada.

Sisendid: Olemasolev mäng.

Andmete töötlemine: Mängu andmete muutmine või täielik kustutamine.

Väljundid: Uute andmetega mäng või siis kustutatud mäng.

Veakäitlus: Hetkel toimivat mängu ei tohi saada kustutada. Mängu uued andmed peavad olema korrektsed.


7)Tiimide sisselogimine. - Prioriteet: Keskmine

Sissejuhatus: Mängijad saabuvad avalehele. Seal nad peavad saama oma tiimi kasutajaga sisse logida.

Sisendid: Tiimi andmed, mille admin on loonud.

Andmete töötlemine: Sisestatud andmed võrreldakse andmebaasis olevatega.

Väljundid: Läbipääs mängule.

Veakäitlus: Sisestatud andmed peavad õiged olema.


8) Vastuste sisestamine - PRIORITEET: KÕRGE

Sissejuhatus: Kui mängijad jõuvad kontrollpunkti, peavad nad saama vastata küsimusele.

Sisendid: Mängija sisestatud vastus.

Andmete töötlemine: Mängija sisestatud vastus võrreldakse andmebaasis oleva vastusega.

Väljundid: Vastus sisestatakse andmebaasi, kus hoitakse, millistele küsimustele nad on õieti või valesti vastanud.

Veakäitlus: Vastus peab olema õige, õiges formaadis ning mitte tühi.


9) Mängijate aja võtt. - Prioriteet: Keskmine

Sissejuhatus: Rakendus peab mõõtma, kui kaua aega läheb mängijatel punktide läbimiseks.

Sisendid: Alustamise aeg, lõpetamise aeg.

Andmete töötlemine: Alustamise ja lõpetamise aega võrreldakse.

Väljundid: Arvutatakse aeg, kui kaua nad läbisid kõiki punkte.

Veakäitlus: Aeg peab olema mõõdetud serveri peal, et kasutaja ei saaks ajaandmetega ise midagi teha.


10) Kaardi näitamine - PRIORITEET: KÕRGE

Sissejuhatus: Kui mängijad saabuvad mängu, peab neil ees olema kaart (kas Google Maps, OpenStreetView või midagi muud seesarnast), kus hakkab toimuma neil mäng.

Sisendid: Admini poolt sisestatud asukoht, kus mäng toimub.

Andmete töötlemine: Luuakse selles asukohas kaart.

Väljundid: Kaart, mida mängijad näevad.

Veakäitlus: Asukohad peavad klappima, kaart peab ikka näitama ka midagi.


11) Punktide näitamine kaardile - PRIORITEET: KÕRGE

Sissejuhatus: Mängijad peavad nägema kaardil järgmist kontrollpunkti, kuhu nad peavad minema. Neid näidatakse ühekaupa.

Sisendid: Punktide järjekord ja asukoht andmebaasis.

Andmete töötlemine: Andmete abil lisatakse punktid kaardile.

Väljundid: Punkt kaardil.

Veakäitlus: Samat punkti ei või kaks korda tulla, punktid peavad tulema õiges järjekorras ja ainult siis, kui kasutaja on eelnevale punktile vastanud.


12) Kasutajate automaatne väljalogimine, kui kõik punktid on läbitud. - prioriteet: madal

Sissejuhatus: Kui kõik punktid on läbitud, peab süsteem kasutajad välja viskama, et nad ei saaks enam midagi muud teha, kuna nende mäng on läbitud.

Sisendid: Kasutajate sessioonid.

Andmete töötlemine: Nad logitakse välja.

Väljundid: Väljalogitud kasutajad.

Veakäitlus: Asi peab töötama.


13) Admini manuaalne väljalogimine - Prioriteet: Keskmine

Sissejuhatus: Admin peab saama välja logida, kui tal on selline soov.

Sisendid: Admini sessioon.

Andmete töötlemine: Admin logitakse välja.

Väljundid: Väljalogitud adminikasutaja.

Veakäitlus: Kui admin on välja loginud, ei tohi saada ligi pääsed adminipaneelile.


14) Süsteemi jälgimine

Sissejuhatus: Adminipaneelil on võimalik jälgida süsteemi jõudlust.

Sisendid: Andmed süsteemi jõudluse kohta.

Andmete töötlemine: Andmed formaaditakse graafikuteks.

Väljundid: Andmed süsteemi kohta.

Veakäitlus: Andmed peavad olema ajakohased.


15) Tiimide tabel

Sissejuhatus: Andmebaasis peab olema tiimide tabel, kuhu on salvestatud andmed meeskondade kohta

Sisendid: Veerud: team_id, team_name, team_password, team_members

Andmete töötlemine: team_id on AUTO_INCREMENT ning PRIMARY KEY

Väljundid: Tabel, mis hoiab meeskondade andmeid

Veakäitlus: "team_members"i veerg peab olema string, kus on komaga eraldatud tiimi liikmed. Need saadakse hiljem kätte


16) Adminikasutaja tabel

Sissejuhatus: Andmebaasis peab olema adminikasutaja tabel, kuhu on salvestatud adminikasutaja andmed, et ta saaks sisse logida.

Sisendid: Veerud: user_id, username, password

Andmete töötlemine: user_id on AUTO_INCREMENT ning PRIMARY KEY

Väljundid: Tabel, mis hoiab adminikasutaja andmeid

Veakäitlus: Tegelikult peaks olema ainult üks kasutaja, kui just manuaalselt teist ei lisata. See luuakse installeerimise käigus.


17) Rakenduse paigaldamine

Sissejuhatus: Rakendus paigaldatakse automaatselt installeerijaga, ehk siis luuakse andmebaasid ja muud vajalikud andmed.

Sisendid: Veebiserver, kuhu see paigaldatakse

Andmete töötlemine: Vajalikud failid pakitakse lahti ja pannakse õigetesse kohtadesse. Skriptid jooksutakse automaatselt.

Väljundid: Töötav rakendus

Veakäitlus: Peab olema korrektselt installeeritud, installi kaust peab olema lõpus automaatselt eemaldatud, et seda ei saaks uuesti jooksutada.
Rakendus peab töötama.



