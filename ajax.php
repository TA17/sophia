<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
session_start();
$team_id = $_SESSION['game']['team_id'];
if (isset($_SESSION['game']['gameid'])) {
    $game_id = $_SESSION['game']['gameid'];
}

require 'classes/Database.class.php';
$db = new Database();
if(isset($_GET['getMarker'])) {

    $sql = "SELECT * FROM game WHERE FIND_IN_SET(".$team_id.",team_ids)";
    if ($result = $db->query($sql)) {

        $stored = $db->query("SELECT * FROM answers WHERE team_id = '$team_id'");

        $team_ids = "";
        foreach ($result as $key => $val) {
            $_SESSION['game']['gameid'] = $val['id'];
            $team_ids .= $val['team_ids'];
        }
        $team_ids = explode(",", $team_ids);
        foreach ($team_ids as $val) {
            if ($team_id == $val) {
                $team = $val;
            }
        }

        $strDestination = $db->query("SELECT * FROM teams WHERE tid = '$team'");
        $strDestination = $strDestination[0]['destination_ids'];
        $stored = $db->query("SELECT * FROM answers WHERE team_id = '$team_id'");
        $arrDestination = explode(",", $strDestination);
        $data = array();
        $arrQuestions = array();
        $saved = true;
        foreach ($arrDestination as $key => $value) {
            $arrTemp = $db->query("SELECT * FROM destination where id = '$value'");
            foreach ($stored as $keys => $val) {
                if ($val['question_id'] == $arrTemp[0]['question_id'] && $saved == true) {
                    $saved = false;
                }
            }
            if ($saved == false) {
                $question_id = $arrTemp[0]['question_id'];
                $question = $db->query("SELECT question FROM questions WHERE q_id = '$question_id'");
                //$question = $question[0]['question'];
                $data = array_merge($arrTemp, $data);
                $data[0]['question'] = $question[0]['question'];
                //$data = array_merge($data, $arrQuestions);
                $saved = true;
            }

        }
        $data = array_reverse($data);
        //print_r($data);
        echo json_encode($data);
    } else {
        echo "meh";
    }

}
if (isset($_POST['setup'])) {

    $result = $db->query("SELECT * FROM gameStorage WHERE team_id = '$team_id'");
    if (is_null($result)) {
        $db->insert("gameStorage", array('team_id' => $team_id, 'game_id' => $game_id));
        echo "Stored";
    } else {
        echo "Mäng juba käib";
    }


}
if (isset($_POST['answer'])) {
    $question_id = $_POST['qid'];
    $answer = $_POST['answer'];
    $sql= "SELECT * FROM gameStorage WHERE team_id = '$team_id'";
    if ($result = $db->query($sql)) {
        foreach ($result as $key => $value) {
            $time = $value['start'];
            $oldAnswerIDs = $value['answers_id'];
        }
        $start_date = new DateTime($time);
        $now = date("Y-m-d H:i:s");
        $since_start = $start_date->diff(new DateTime($now));
        $between = $since_start->format("%H:%I:%S");

        $id = $db->insert("answers", array('team_id' => $team_id, 'game_id' => $game_id, 'question_id' => $question_id, 'answer' => $answer, 'time' => $between));
        $newid = $oldAnswerIDs.$id.",";
        $db->update('gameStorage', $arrayName = array('answers_id' =>  $newid), "team_id = '$team_id'");
        echo "Stored";
    }
    if (isset($_POST['end'])) {
        $db->update('gameStorage', $arrayName = array('end' =>  "NOW()"), "team_id = '$team_id'");
        echo "The End";
    }
}
