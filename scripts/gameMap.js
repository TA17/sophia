(function($){
    var domain = window.location.origin;
    var markers;
    var markerLocations;
    var markersUsed = 0;
    var answered = false;
    var marker;
    var userCircle;
    var maxDistance = 6;
    var apiURL = domain + "/ajax.php";
    $.ajax(apiURL + '?getMarker=true', {
        contentType: 'application/json',
        dataType: 'json',
        type: "POST",
        data: {'getMarker': '21'},
        success: function(data) {
            markers = data.length - 1;
            console.log("Markers: " + markers);
            markerLocations = data;
            $('#lblq').text(markerLocations[0].question)
            setMarker(markerLocations[0].lat, markerLocations[0].lon);
        },
    });
    var map = L.map('gamemap').fitWorld();


    L.tileLayer('https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}', {
    }).addTo(map);

    /*L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
        attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
        id: 'mapbox.streets',
        accessToken: 'pk.eyJ1IjoibHVtaW5lbyIsImEiOiJjanZudHdsMmwxYzc3M3pwaWYxYjhnbnpqIn0.ox6WfjaRWBxvgUKEJ1hXww',
    }).addTo(map);*/
    map.locate({
        setView: true,
        maxZoom: 18,
        watch: true,
        enableHighAccuracy: true,

    });
    function userLocationMarker(e) {
        if (userCircle != null) {
            map.removeLayer(userCircle)
        }
        userCircle = L.circle(e.latlng, {
           color: 'red',
           fillColor: 'red',
           fillOpacity: 0.5,
           radius: 3,
       }).addTo(map);
    }
    function setMarker(latitude, longitude) {
        marker = L.marker([latitude, longitude]).addTo(map);
        console.log(markers);
        $.ajax({
            url: apiURL,
            type: 'POST',
            data: {'setup': true}
        });
        marker.on('click', function(clickedMarker) {
            $('#exampleModal').modal({ backdrop: 'static',keyboard: false});
        });
    }
    function handleCurrentNavEnd(oldMarker) {
        if (markers == (markersUsed)) {
            map.removeLayer(marker)
            map.remove();
            $('#gamemap').css('display', 'none');
            $('#game-over').show();
        } else {
            markerLocations.shift();
            $('#lblq').text(markerLocations[0].question)
            markersUsed++
            updateMarker(marker, markerLocations[0].lat, markerLocations[0].lon);
        }

    }
    function checker() {
        if (marker != null) {
            from = userCircle.getLatLng();
            to = marker.getLatLng();
            markerDistance = to.distanceTo(from);
            if(maxDistance >= markerDistance) {
                $('#exampleModal').modal({ backdrop: 'static',keyboard: false});

            } else {
                console.log("not here")
            }
        }
    }
    function updateMarker(marker, latitude, longitude) {
        marker.setLatLng([latitude, longitude]);
    }
    function onLocationFound(e) {
        userLocationMarker(e)
        userCircle.bindPopup("Asukoht: " + e.latlng).openPopup();
        checker();
    }

    function onLocationError(e) {
        alert(e.message);
    }
    $('#btnQuest').on('click', function() {
        var answer = $("#exampleModal #question").val().trim()
        if (answer == "") {
            $('#question').addClass('is-invalid');
        } else {
            $('#question').removeClass('is-invalid')
            var quest_id = markerLocations[0].question_id;
            $('#exampleModal').modal('hide');
            $.ajax({
                url: apiURL,
                type: 'POST',
                data: {'answer': answer, 'qid': quest_id},
                success: function(data) {
                    handleCurrentNavEnd(marker);
                },
            });
        }

    });
    map.on('locationerror', onLocationError);
    map.on('locationfound', onLocationFound);
})(jQuery);
