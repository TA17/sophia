(function($){
    var apiURL = 'https://lunar.icu/sophia/ajax.php';
    var defaultLat = 58.9995614;
    var defaultLon = 24.8008137;
    var defaultZoom = 18;
    var mymap = L.map('gamemap').setView([defaultLat, defaultLon], defaultZoom);
    var markerLocations = [];
    var markers;
    var markersUsed = 0;
    var markerDistance = 0;
    var maxDistance = 3;
    var from;
    var to;
    L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
        attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
        id: 'mapbox.streets',
        accessToken: 'pk.eyJ1IjoibHVtaW5lbyIsImEiOiJjanZudHdsMmwxYzc3M3pwaWYxYjhnbnpqIn0.ox6WfjaRWBxvgUKEJ1hXww',
    }).addTo(map);

    var userCircle = L.circle([defaultLat, defaultLon], {
        color: 'red',
        fillColor: 'red',
        fillOpacity: 0.5,
        radius: 3,
    }).addTo(mymap);

    var marker;

    if ('geolocation' in navigator) {
        getUserLocation();
    }

    function getUserLocation() {
        navigator.geolocation.getCurrentPosition(function(position) {
            var lat = position.coords.latitude;
            var lon = position.coords.longitude;
            updateMapLocation(lat, lon);
            setUpLocationListener();
            initializeMarkers();
        });
    }

    function updateMapLocation(latitude, longitude) {
        mymap.setView([latitude, longitude]);
        setUserMarker(latitude, longitude);
    }

    function setUpLocationListener() {
        navigator.geolocation.watchPosition(function(position) {
            updateMapLocation(position.coords.latitude, position.coords.longitude);
        });
    }

    function setUserMarker(latitude, longitude) {
        userCircle.setLatLng([latitude, longitude]);
        userCircle.on('click', function() {
            userCircle.bindPopup("Kaugus: " + Math.round(markerDistance) + "m").openPopup();
        });
    }

    function checker() {
        from = userCircle.getLatLng();
        to = marker.getLatLng();
        markerDistance = to.distanceTo(from);
        if(maxDistance >= markerDistance) {
            console.log("yeeeet")
            userCircle.bindPopup("Olen kohal").openPopup();
            handleCurrentNavEnd(marker);
        } else {
            console.log("not here")
        }
    }


    function setMarker(latitude, longitude) {
        marker = L.marker([latitude, longitude]).addTo(mymap);
        console.log(markers);
        marker.on('click', function(clickedMarker) {
            console.log(markersUsed);
            handleCurrentNavEnd(clickedMarker);
        });
    }

    function handleCurrentNavEnd(oldMarker) {
        if (markers < markersUsed) {
            mymap.removeLayer(marker)
            userCircle.bindPopup('Mäng Läbi :^)').openPopup();
        } else {
            markerLocations.shift();
            updateMarker(marker, markerLocations[0].lat, markerLocations[0].lon);
        }
        markersUsed++
    }

    function updateMarker(marker, latitude, longitude) {
        marker.setLatLng([latitude, longitude]);
    }

    function initializeMarkers() {
        $.ajax(apiURL + '?getMarker=true', {
            contentType: 'application/json',
            dataType: 'json',
            success: function(data) {
                markers = data.length - 1;
                markerLocations = data;
                setMarker(markerLocations[0].lat, markerLocations[0].lon);
            },
        });
    }
    setInterval(checker, 1500);
})(jQuery);
