<?php
include __DIR__.'/../inc/config.inc.php';
class Database
{
    public $_connection;

    private $_host;

    private $_user;

    private $_pass;

    private $_dbName;

    private $_port;

    public function __construct()
    {
        global $db_host,$db_user,$db_password,$db_database;

        $this->_host = $db_host;
        $this->_user = $db_user;
        $this->_pass = $db_password;
        $this->_dbName = $db_database;
        $this->_port = 3306;

        try {
            $this->_connect();
            $this->_disconnect();
        } catch (Exception $ex) {
            die($ex->getMessage());
        }
    }

    public function _connect()
    {
        $this->_connection = mysqli_connect($this->_host, $this->_user, $this->_pass, $this->_dbName, $this->_port);
        if (! $this->_connection) {
            throw new mysqli_sql_exception();
        }

        mysqli_set_charset($this->_connection, "utf8");
    }

    public function _disconnect()
    {
        if ($this->_connection) {
            mysqli_commit($this->_connection);
            mysqli_close($this->_connection);
        }
    }

    public function _dbSql($sql)
    {

        $this->_connect();
        mysqli_query($this->_connection, $sql);
        $lastID = mysqli_insert_id($this->_connection);
        $error = mysqli_error($this->_connection);
        $this->_disconnect();
        if ($error) {
            //throw new mysqli_sql_exception();
            throw new Exception($error);
        }

        return $lastID;
    }

    public function query($qry, $showErrors = true)
    {

        $res = array();

        $this->_connect();
        $result = mysqli_query($this->_connection, $qry);
        if ($result) {
            $count = mysqli_num_rows($result);
            if ($count > 0) {
                while ($r = mysqli_fetch_assoc($result)) {
                    $res[] = $r;
                }
                $this->_disconnect();


                return $res;
            }
        }
        $error = mysqli_error($this->_connection);
        $this->_disconnect();
        if ($showErrors && $error) {
            throw new mysqli_sql_exception();
        }


        return null;
    }

    public function checkIfDatabaseExist($database)
    {


        $sql = "SHOW DATABASES LIKE '$database'";

        $result = $this->query($sql);

        if ($result) {
            return true;
        }

        return false;
    }

    public function checkIfTableExists($table)
    {
        $res = $this->query("SELECT table_name FROM information_schema.tables WHERE table_schema = '{$this->_dbName}' AND table_name = '$table';");
        if ($res) {
            return true;
        }

        return false;
    }
    public function insert($table, $data)
    {
        $sql = "INSERT INTO $table(";
        foreach ($data as $i => $k) {
            $sql .= "`" . $i . "`" . ", ";
        }
        $sql = substr($sql, 0, - 2);
        $sql .= ") VALUES(";
        foreach ($data as $i => $k) {
            $sql .= "'" . $k . "'" . ", ";
        }
        $sql = substr($sql, 0, - 2);
        $sql .= ")";

        return $this->_dbSql($sql);
    }

    /*
     * return null
     */
    public function update($table, $data, $condition)
    {
        $sql = "UPDATE $table SET ";
        foreach ($data as $i => $k) {
            $sql .= "`" . $i . "`='" . $k . "', ";
        }
        $sql = substr($sql, 0, - 2);
        $sql .= " WHERE $condition";
        $this->_dbSql($sql);

        return true;
    }

    public function delete($table, $where = "")
    {
        $sql = "DELETE FROM `$table`";
        if ($where) {
            $sql .= " where $where";
        }
        $this->_dbSql($sql);

        return true;
    }
}
